import React, { useState } from 'react'
import Input from './core/Input'
import TextArea from './core/TextArea'
import Submit from './core/Submit'
import './App.css';

function App() {
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [message, setMessage] = useState("")

  return (
    <div>
      <header>
        <h1>FELLASOFT</h1>
        {/* <div class='links'>
          <a href="#about">About</a>
          <a href="#products">Products</a>
          <a href="#contact">Contact</a>
        </div> */}
      </header>
      <header id="no-float">
        <h1>FELLASOFT</h1>
        {/* <div class='links'>
          <a href="#about">About</a>
          <a href="#products">Products</a>
          <a href="#contact">Contact</a>
        </div> */}
      </header>
      {/* <section id="about">
        <summary>
          <h1>About</h1>
        </summary>
      </section>
      <section id="products">
        <summary>
          <h1>Products</h1>
        </summary>
      </section> */}
      <section id="contact">
        <summary>
          <h1>Contact</h1>
          <div>
            <div style={{ padding: 10 }}>
              <Input placeholder='your name' value={name} onChange={e => setName(e.target.value)} />
            </div>
            <div style={{ padding: 10 }}>
              <Input placeholder='your email' value={email} onChange={e => setEmail(e.target.value)} />
            </div>
            <div style={{ padding: 10 }}>
              <TextArea placeholder='message' value={message} onChange={e => setMessage(e.target.value)} />
            </div>
            <div style={{textAlign: 'right'}}>
              <Submit placeholder='message' value='Send' onClick={e => sendEmail(name, email, message)} />
            </div>
          </div>
        </summary>
      </section>
    </div>
  );
}

function sendEmail(name, email, message) {
  console.log("egwegw")
	window.emailjs.send(
  	'zoho', 'template_K8t73voV',
  	{message_html: message, from_name: name, reply_to: email}
  	).then(res => {
    	console.log('Email successfully sent!')
  	})
  	.catch(err => console.error('Oh well, you failed. Here some thoughts on the error that occured:', err))
  }

export default App;
